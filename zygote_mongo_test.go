package swmongo_test

import (
	"fmt"
	"testing"

	"gitlab.com/alexia.shaowei/swmongo"
	"go.mongodb.org/mongo-driver/bson"
)

var mon *swmongo.MongoOperator
var err error

var data bson.D

func init() {
	mon, err = swmongo.New("127.0.0.1", 27017)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("MongoDB is alive: %t\n", mon.IsAlive())

	data = bson.D{
		{Key: "Name", Value: "S.W."},
		{Key: "Location", Value: "Nowhere"},
		{Key: "Series", Value: bson.A{1, 2, 3, 4, 5, 6, 7, 8, 9}},
	}
}

// go test -v -cover -short -bench .
func BenchmarkInsertOne(b *testing.B) {
	// b.Skip()
	for i := 0; i < b.N; i++ {
		mon.InsertOne("test", "data", data)
	}

}
