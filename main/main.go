package main

import (
	"fmt"

	"gitlab.com/alexia.shaowei/swmongo"
	"go.mongodb.org/mongo-driver/bson"
)

var mon *swmongo.MongoOperator
var err error

var data bson.D

func init() {
	mon, err = swmongo.New("127.0.0.1", 27017)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("MongoDB is alive: %t\n", mon.IsAlive())

	data = bson.D{
		{Key: "Name", Value: "S.W."},
		{Key: "Location", Value: "Nowhere"},
		{Key: "Series", Value: bson.A{1, 2, 3, 4, 5, 6, 7, 8, 9}},
	}
}

func main() {
	fmt.Println("main::main()")

	v, e := mon.InsertOne("test", "data", data)
	fmt.Println(v, e)
}
