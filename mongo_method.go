package swmongo

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (ref *MongoOperator) IsAlive() bool {

	if ref.monClient == nil {
		return false
	}

	ctx, ctxCancel := context.WithTimeout(context.TODO(), time.Second*5)
	defer ctxCancel()

	if err := ref.monClient.Ping(ctx, nil); err != nil {
		return false
	}

	return true
}

func (ref *MongoOperator) Disconnect() bool {
	if err := ref.monClient.Disconnect(context.TODO()); err != nil {
		return false
	}

	return true
}

func (ref *MongoOperator) GetIndexNames(databaseName string, collectionName string) ([]string, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	cursor, err := collection.Indexes().List(context.TODO())
	if err != nil {
		return []string{}, err
	}

	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		return []string{}, err
	}

	indexes := make([]string, 0)
	for _, val := range results {
		v, ok := val["name"].(string)
		if ok {
			indexes = append(indexes, v)
		}
	}

	return indexes, nil
}

func (ref *MongoOperator) GetIndexes(databaseName string, collectionName string) ([]string, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	cursor, err := collection.Indexes().List(context.TODO())
	if err != nil {
		return []string{}, err
	}

	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		return []string{}, err
	}

	indexes := make([]string, 0)
	for _, val := range results {
		v := val["key"].(primitive.M)
		for k := range v {
			indexes = append(indexes, fmt.Sprintf("%v", k))
		}
	}

	return indexes, nil
}

func (ref *MongoOperator) DropOneIndex(databaseName string, collectionName string, key string) error {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	_, err := collection.Indexes().DropOne(context.TODO(), key)
	if err != nil {
		return err
	}

	return nil
}

func (ref *MongoOperator) DropIndexes(databaseName string, collectionName string) error {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	_, err := collection.Indexes().DropAll(context.TODO())
	if err != nil {
		return err
	}

	return nil
}

func (ref *MongoOperator) CreateIndexes(databaseName string, collectionName string, keys []string) ([]string, error) {

	if len(keys) == 0 {
		return []string{}, fmt.Errorf("fail to create indexes")
	}

	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	mods := make([]mongo.IndexModel, 0)
	for i := 0; i < len(keys); i++ {
		mods = append(mods, mongo.IndexModel{
			Keys:    bson.M{keys[i]: 1},
			Options: nil,
		})
	}

	idxs, err := collection.Indexes().CreateMany(context.TODO(), mods)
	if err != nil {
		return []string{}, err
	}

	return idxs, nil
}

func (ref *MongoOperator) InsertOne(databaseName string, collectionName string, docObject interface{}) (string, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	insertRes, err := collection.InsertOne(context.TODO(), docObject)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%v", insertRes.InsertedID), nil
}

func (ref *MongoOperator) InsertMany(databaseName string, collectionName string, docObjectSlice []interface{}) (int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	insertManyRes, err := collection.InsertMany(context.TODO(), docObjectSlice)
	if err != nil {
		return -1, err
	}

	return len(insertManyRes.InsertedIDs), nil
}

func (ref *MongoOperator) UpdateOne(databaseName string, collectionName string, filter interface{}, update interface{}) (int, int, int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	updateRes, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return -1, -1, -1, err
	}

	return int(updateRes.ModifiedCount), int(updateRes.UpsertedCount), int(updateRes.MatchedCount), err
}

func (ref *MongoOperator) UpdateMany(databaseName string, collectionName string, filter interface{}, update interface{}) (int, int, int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	updateRes, err := collection.UpdateMany(context.TODO(), filter, update)
	if err != nil {
		return -1, -1, -1, err
	}

	return int(updateRes.ModifiedCount), int(updateRes.UpsertedCount), int(updateRes.MatchedCount), err
}

func (ref *MongoOperator) DeleteOne(databaseName string, collectionName string, filter interface{}) (int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	deleteRes, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return -1, err
	}

	return int(deleteRes.DeletedCount), nil
}

func (ref *MongoOperator) DeleteMany(databaseName string, collectionName string, filter interface{}) (int, error) {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	deleteManyRes, err := collection.DeleteMany(context.TODO(), filter)
	if err != nil {
		return -1, err
	}

	return int(deleteManyRes.DeletedCount), nil
}

func (ref *MongoOperator) Drop(databaseName string, collectionName string) error {
	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	err := collection.Drop(context.TODO())
	if err != nil {
		return err
	}

	return nil
}

func (ref *MongoOperator) FindOne(databaseName string, collectionName string, filter interface{}, objPointer interface{}) error {
	if objPointer == nil {
		return fmt.Errorf("nil pointer")
	}

	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	singleResult := collection.FindOne(context.TODO(), filter)

	err := singleResult.Decode(objPointer)
	if err != nil {
		return err
	}

	return nil
}

func (ref *MongoOperator) FindMany(databaseName string, collectionName string, maxRows int, filter interface{}, objListPointer interface{}) error {
	if objListPointer == nil {
		return fmt.Errorf("nil pointer")
	}

	if maxRows <= 0 {
		return fmt.Errorf("maxRows must be greater than 0")
	}

	findOpts := options.Find()
	findOpts.SetLimit(int64(maxRows))

	collection := ref.monClient.Database(databaseName).Collection(collectionName)

	cursor, err := collection.Find(context.TODO(), filter, findOpts)
	if err != nil {
		return err
	}

	defer cursor.Close(context.TODO())

	if errCur := cursor.All(context.TODO(), objListPointer); errCur != nil {
		return errCur
	}

	return nil
}
